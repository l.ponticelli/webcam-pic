$(function() {
  var video = $("#video")[0],
    support = true;
  (streaming = false),
    (width = 640),
    track= null,
    (cvs = document.createElement("canvas")),
    (ctx = cvs.getContext("2d"));

  $("#message").hide();

  //////FILE
  if (!window.File || !window.FileReader) {
    //return alert("Oops!\nYour browser isn't quite compatible with this pen.");
  } else {
    $(".my_input").bind("change", function() {
      var file = this.files[0],
        reader = new FileReader();
      reader.onload = function(e) {
        $("#img_upload").attr("src", e.target.result);
      };

      reader.readAsDataURL(file);
    });
    //////WEBCAM

    navigator.getMedia =
      navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia;

    if (!navigator.getMedia) {
      support = false;
    }
  }

  navigator.getMedia(
    { video: true, audio: false },
    function(stream) {
      if (navigator.mozGetUserMedia) video.mozSrcObject = stream;
      else {
        var vu = window.URL || window.webkitURL;
        video.src = vu.createObjectURL(stream);
      }
      video.play();
      track = stream.getVideoTracks()[0];
    },
    function(error) {
      support = false;
      if (window.console) console.error(error);
    }
  );

  if (support) {
    video.addEventListener(
      "canplay",
      function(ev) {
        if (!streaming) {
          console.log("width", width);
          height = video.videoHeight / (video.videoWidth / width);
          video.setAttribute("width", width);
          video.setAttribute("height", height);
          streaming = true;
        }
      },
      false
    );

    $("#holder").on("click", function() {
      cvs.width = video.width;
      cvs.height = video.height;
      ctx.drawImage(video, 0, 0, cvs.width, cvs.height);

      $("#preview").attr("src", cvs.toDataURL());
      //track.stop();
    });
  } else {
    $("#webcam").hide();
    $("#message").show();
  }
});
